from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class ResPartner(models.Model):
    """inherit res.partner"""
    _inherit = 'res.partner'

    @api.model
    def tes_method(self, data):
        """ Return Data """
        if data:
            return data
        return False
    
    @api.model
    def change_partner_name(self, data):
        """ Change Partner name based on user input """
        Partner = self.env['res.partner']
        if data:
            for rec in data:
                if rec.get('id'):
                    partner = Partner.browse(rec.get('id'))
                else:
                    raise ValidationError(_(
                        'id cant be empty'
                    ))
                if rec.get('name'):
                    partner.write({
                        'name':rec.get('name')
                    })
                else:
                    raise ValidationError(_(
                        'name cant be empty'
                    ))
                return data
        return False