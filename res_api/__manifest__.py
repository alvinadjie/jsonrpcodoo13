{
    'name': 'Res API Jsonrpc',
    'category': 'Extra',
    "author": "Portcities",
    'website': 'https://www.portcities.net',
    'version': '13.0.1.0.0',
    'description': '''
Custom Module for JSON-RPC API

''',
    'depends': ['base'],
    'data': [],
    'installable': True,
    'auto_install': False,
}
